import React, { useContext } from "react";
import { Store } from "../utils/context/store";

const Dashboard = () => {
  const { subscriptions } = useContext(Store);
  const pack1Count = subscriptions.filter(
    (elm) => elm.package === "Plan 1"
  ).length;
  const pack2Count = subscriptions.filter(
    (elm) => elm.package === "Plan 2"
  ).length;
  const pack3Count = subscriptions.filter(
    (elm) => elm.package === "Plan3"
  ).length;
  const pack6Count = subscriptions.filter(
    (elm) => elm.package === "Plan 6"
  ).length;
  const pack12Count = subscriptions.filter(
    (elm) => elm.package === "Plan 12"
  ).length;
  const packUnlimitedCount = subscriptions.filter(
    (elm) => elm.package === "Plan Unlimited"
  ).length;

  return (
    <div className="card-container">
      <div className="card-wrapper">
        <div className="card">
          <label for="active-user" style={{ fontWeight: "bold" }}>
            Packages({subscriptions?.length})
          </label>
          <span
            id="active-user"
            style={{ fontWeight: "bold", marginLeft: "10px" }}
          >
            Subscribers(In number)
          </span>
        </div>
        <div className="card">
          <label for="active-user">Plan 1 : </label>
          <meter
            id="active-user"
            min="0"
            max={subscriptions?.length}
            low={subscriptions?.length * 0.1}
            optimum={subscriptions?.length * 0.4}
            high={subscriptions?.length * 0.6}
            value={pack1Count}
          />
        </div>
        <div className="card">
          <label for="active-user">Plan 2 : </label>
          <meter
            id="active-user"
            min="0"
            max={subscriptions?.length}
            low={subscriptions?.length * 0.1}
            optimum={subscriptions?.length * 0.4}
            high={subscriptions?.length * 0.6}
            value={pack2Count}
          />
        </div>
        <div className="card">
          <label for="active-user">Plan 3 : </label>
          <meter
            id="active-user"
            min="0"
            max={subscriptions?.length}
            low={subscriptions?.length * 0.1}
            optimum={subscriptions?.length * 0.4}
            high={subscriptions?.length * 0.6}
            value={pack3Count}
          />
        </div>
        <div className="card">
          <label for="active-user">Plan 6 : </label>
          <meter
            id="active-user"
            min="0"
            max={subscriptions?.length}
            low={subscriptions?.length * 0.1}
            optimum={subscriptions?.length * 0.4}
            high={subscriptions?.length * 0.6}
            value={pack6Count}
          />
        </div>
        <div className="card">
          <label for="active-user">Plan 12 : </label>
          <meter
            id="active-user"
            min="0"
            max={subscriptions?.length}
            low={subscriptions?.length * 0.1}
            optimum={subscriptions?.length * 0.4}
            high={subscriptions?.length * 0.6}
            value={pack12Count}
          />
        </div>
        <div className="card">
          <label for="active-user">Plan Unlimited : </label>
          <meter
            id="active-user"
            min="0"
            max={subscriptions?.length}
            low={subscriptions?.length * 0.1}
            optimum={subscriptions?.length * 0.4}
            high={subscriptions?.length * 0.6}
            value={packUnlimitedCount}
          />
        </div>
      </div>
    </div>
  );
};

export default Dashboard;

import React from "react";

const FilterationSorting = ({
  filterData,
  setFilterData,
  setIsActive,
  setSorting,
}) => {
  return (
    <div className="filter-container">
      <div className="filter-wrapper">
        <input
          type="text"
          placeholder="Search by name..."
          value={filterData}
          onChange={(e) => setFilterData(e.target.value)}
        />
        <label
          style={{ fontSize: "0.7rem", paddingLeft: "5px", opacity: "0.8" }}
        >
          Filter Name
        </label>
      </div>
      <div className="filter-wrapper">
        <select onChange={(e) => setIsActive(e.target.value)}>
          <option value="">All Users</option>
          <option value="1">Active Users</option>
          <option value="0">In Active Users</option>
        </select>
        <label
          style={{ fontSize: "0.7rem", paddingLeft: "5px", opacity: "0.8" }}
        >
          Filter Active Users
        </label>
      </div>

      <div className="filter-wrapper">
        <select onChange={(e) => setSorting(e.target.value)}>
          <option value="">Sort By</option>
          <option value="first_name">Name</option>
          <option value="address">Address</option>
          <option value="country">Country</option>
          <option value="email">Email</option>
          <option value="join_date">Join Date</option>
        </select>
        <label
          style={{ fontSize: "0.7rem", paddingLeft: "5px", opacity: "0.8" }}
        >
          Sort data
        </label>
      </div>
    </div>
  );
};

export default FilterationSorting;

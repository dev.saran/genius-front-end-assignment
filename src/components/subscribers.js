import React, { useContext, useState } from "react";
import { Store } from "../utils/context/store";
import Dashboard from "./dashboard";
import FilterationSorting from "./filteration-sorting";
import SubscribersHeader from "./subscribersHeader";
import SubscriptionModel from "./subscriptionModel";
const Subscribers = () => {
  const { subscriptions, users } = useContext(Store);
  const [filterData, setFilterData] = useState("");
  const [isActive, setIsActive] = useState("");
  const [sorting, setSorting] = useState("");
  const [loadMore, setLoadMore] = useState(16);
  const [modal, setModal] = useState(false);
  const [sortedData, setSortedData] = useState({});

  const packageList = [
    ...new Map(subscriptions.map((item) => [item["package"], item])).values(),
  ];

  function showModal(id) {
    subscriptions
      ?.filter((elm) => elm.user_id == id)
      .map((x) => {
        setSortedData(x);
      });

    if (sortedData.hasOwnProperty("user_id")) {
      setModal(true);
    }
    return;
  }

  return (
    <>
      <Dashboard />
      <SubscribersHeader />
      <FilterationSorting
        filterData={filterData}
        setFilterData={setFilterData}
        setIsActive={setIsActive}
        packageList={packageList}
        setSorting={setSorting}
      />
      <div className="table-container">
        <table>
          <thead>
            <tr>
              <th>SN</th>
              <th>User Name</th>
              <th>Full Name</th>
              <th>Address</th>
              <th>Country</th>
              <th>Email</th>
              <th>Join Date</th>
              <th>View Subscription</th>
            </tr>
          </thead>
          <tbody>
            {users
              .filter((elm2) => {
                if (filterData === "") {
                  return elm2;
                } else if (filterData !== "") {
                  return (
                    elm2.first_name
                      .toLowerCase()
                      .indexOf(filterData.toLowerCase()) !== -1
                  );
                } else {
                  return elm2;
                }
              })
              .filter((elm) => {
                if (isActive === "") {
                  return elm;
                } else {
                  return elm.active === isActive;
                }
              })
              .sort((a, b) => {
                if (sorting === "") return a;
                return a[sorting].localeCompare(b[sorting]);
              })
              .map((elm, index) => {
                return (
                  <tr key={index}>
                    <td>{index + 1}</td>
                    <td>{elm.username}</td>
                    <td>{`${elm.first_name} ${
                      elm.middle_name ? elm.middle_name : ""
                    } ${elm.last_name}`}</td>
                    <td>{elm.address}</td>
                    <td>{elm.country}</td>
                    <td>{elm.email}</td>
                    <td>{elm.join_date}</td>
                    <td>
                      <button
                        className="btn-loadmore"
                        onClick={() => showModal(elm.id)}
                      >
                        View
                      </button>
                    </td>
                  </tr>
                );
              })
              .splice(0, loadMore)}
          </tbody>
        </table>
        {loadMore < users.length && (
          <button
            className="btn-loadmore"
            onClick={() => setLoadMore(loadMore + 16)}
          >
            Show More
          </button>
        )}
        {loadMore > 16 && (
          <button
            className="btn-loadmore"
            onClick={() => setLoadMore(loadMore - 16)}
          >
            Show Less
          </button>
        )}
      </div>

      {modal && (
        <SubscriptionModel
          modal={modal}
          setModal={setModal}
          data={sortedData}
          setSortedData={setSortedData}
        />
      )}
    </>
  );
};

export default Subscribers;

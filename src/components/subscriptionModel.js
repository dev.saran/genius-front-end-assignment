const SubscriptionModel = ({ modal, data, setModal, setSortedData }) => {
  function handleCloseModal() {
    setSortedData({});
    setModal(false);
  }
  return (
    <div className="subscription-modal" style={{ right: modal ? "0" : "-300" }}>
      <div className="modal-container" style={{ textAlign: "center" }}>
        <h5>Package : {data?.package}</h5>
        <p>
          <h5>Expires on : {data?.expires_on}</h5>
        </p>
        <button className="btn-modal" onClick={handleCloseModal}>
          Go Back
        </button>
      </div>
    </div>
  );
};

export default SubscriptionModel;

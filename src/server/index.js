import subscription from "../data/subscriptions.json";
import user from "../data/users.json";

export const subscriptionServer = async () => {
  const data = await subscription;
  return data;
};
export const userServer = async () => {
  const data = await user;
  return data;
};
